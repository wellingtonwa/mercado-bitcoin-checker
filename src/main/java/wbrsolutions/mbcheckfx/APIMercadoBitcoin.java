/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbrsolutions.mbcheckfx;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import wbrsolutions.mbcheckfx.model.Orderbook;
import wbrsolutions.mbcheckfx.model.Ticker;
import wbrsolutions.mbcheckfx.model.Trade;
import wbrsolutions.mbcheckfx.model.enumerated.ApiMethodEnum;
import wbrsolutions.mbcheckfx.model.enumerated.CoinEnum;
import wbrsolutions.mbcheckfx.model.util.HtmlUtil;

/**
 *
 * @author Wellington Wagner (wellingtonwa@gmail.com)
 */
public class APIMercadoBitcoin {

    private static final String URL_BASE_API = "https://www.mercadobitcoin.com.br/api/%s/%s/";

    private APIMercadoBitcoin() {
    }

    public static Orderbook getOrderbook(final CoinEnum coinEnum){
        final ObjectMapper mapper = new ObjectMapper();
        try {
            final String json = HtmlUtil.getHtml(String.format(URL_BASE_API, coinEnum.acronimo(), ApiMethodEnum.ORDERBOOK.apiMethod()));
            return mapper.readValue(json, Orderbook.class);
        } catch (final Exception e) {

        }
        return new Orderbook();
    }

    public static Ticker getTicker(final CoinEnum coinEnum) {
        final ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true);
            final String json = HtmlUtil.getHtml(String.format(URL_BASE_API, coinEnum.acronimo(), ApiMethodEnum.TICKER.apiMethod()));
            return mapper.readValue(json, Ticker.class);
        } catch (final Exception e) {

        }
        return new Ticker();
    }

    public static List<Trade> getTrades(final CoinEnum coinEnum) {
        final ObjectMapper mapper = new ObjectMapper();
        try {
            final String json = HtmlUtil.getHtml(String.format(URL_BASE_API, coinEnum.acronimo(), ApiMethodEnum.TRADES.apiMethod()));
            return mapper.readValue(json, new TypeReference<List<Trade>>() {
            });
        } catch (final Exception ignored) {
        }
        return new ArrayList<>();
    }

}
