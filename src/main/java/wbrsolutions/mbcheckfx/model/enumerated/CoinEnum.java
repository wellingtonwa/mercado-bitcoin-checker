package wbrsolutions.mbcheckfx.model.enumerated;

public enum CoinEnum {

    BITCOIN("BTC", "Bitcoin"),
    LITECOIN("LTC", "Litecoin"),
    BCHASH("BCH", "BCash"),
    RIPPLE("XRP","XRP (Ripple)"),
    ETHEREUM("ETH", "Ethereum");

    private final String acronimo;
    private final String description;

    CoinEnum(final String acronimo, final String description) {
        this.acronimo = acronimo;
        this.description = description;
    }

    public String acronimo() {
        return acronimo;
    }

    public String description() {
        return description;
    }
}
