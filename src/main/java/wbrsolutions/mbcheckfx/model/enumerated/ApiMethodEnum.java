package wbrsolutions.mbcheckfx.model.enumerated;

public enum ApiMethodEnum {

    TICKER("ticker", "resumo de operações executadas"),
    ORDERBOOK("orderbook", "livro de negociações, ordens abertas de compra e venda"),
    TRADES("trades", "histórico de operações executadas");

    private final String apiMethod;
    private final String description;

    ApiMethodEnum(final String apiMethod, final String description) {
        this.apiMethod = apiMethod;
        this.description = description;
    }

    public String apiMethod() {
        return apiMethod;
    }

    public String description() {
        return description;
    }
}
