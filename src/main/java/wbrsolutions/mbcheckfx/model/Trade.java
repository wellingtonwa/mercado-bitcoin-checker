/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package wbrsolutions.mbcheckfx.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Wellington Wagner (wellingtonwa@gmail.com)
 */
public class Trade {
    
    private Long date;
    private BigDecimal price;
    private BigDecimal amount;
    private Long tid;
    private String type;

    public Long getDate() {
        return date;
    }

    public void setDate(final Long date) {
        this.date = date;
    }

    @JsonIgnore
    public String getData() {
        return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date(date*1000));
    }
    
    
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        if (price!=null) {
            price = price.setScale(5, RoundingMode.HALF_EVEN);
        }
        this.price = price;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        if (amount!=null) {
            amount = amount.setScale(5, RoundingMode.HALF_EVEN);
        }
        this.amount = amount;
    }

    public Long getTid() {
        return tid;
    }

    public void setTid(final Long tid) {
        this.tid = tid;
    }
    
    @JsonIgnore
    public String getTipo() {
        if("sell".equals(type))
            return "Venda";
        else if("buy".equals(type))
            return "Compra";
        
        return type;
    }
    
    
    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }
    
    
}
