/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package wbrsolutions.mbcheckfx.model;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonRootName;

/**
 *
 * @author Wellington Wagner (wellingtonwa@gmail.com)
 */
@JsonRootName("ticker")
public class Ticker {
    private BigDecimal high;
    private BigDecimal low;
    private BigDecimal vol;
    private BigDecimal last;
    private BigDecimal buy;
    private BigDecimal sell;
    private BigDecimal date;

    public BigDecimal getHigh() {
        return high;
    }

    public void setHigh(final BigDecimal high) {
        this.high = high;
    }

    public BigDecimal getLow() {
        return low;
    }

    public void setLow(final BigDecimal low) {
        this.low = low;
    }

    public BigDecimal getVol() {
        return vol;
    }

    public void setVol(final BigDecimal vol) {
        this.vol = vol;
    }

    public BigDecimal getLast() {
        return last;
    }

    public void setLast(final BigDecimal last) {
        this.last = last;
    }

    public BigDecimal getBuy() {
        return buy;
    }

    public void setBuy(final BigDecimal buy) {
        this.buy = buy;
    }

    public BigDecimal getSell() {
        return sell;
    }

    public void setSell(final BigDecimal sell) {
        this.sell = sell;
    }

    public BigDecimal getDate() {
        return date;
    }

    public void setDate(final BigDecimal date) {
        this.date = date;
    }
    
    
}
