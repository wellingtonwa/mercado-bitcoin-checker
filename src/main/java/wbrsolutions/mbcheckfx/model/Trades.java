/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package wbrsolutions.mbcheckfx.model;

import com.fasterxml.jackson.annotation.JsonRootName;
import java.util.List;

/**
 *
 * @author Wellington Wagner (wellingtonwa@gmail.com)
 */
@JsonRootName("trades")
public class Trades {
    
    private List<Trade> trades;

    public List<Trade> getTrades() {
        return trades;
    }

    public void setTrades(final List<Trade> trades) {
        this.trades = trades;
    }
    
}
