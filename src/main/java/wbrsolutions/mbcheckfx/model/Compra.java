/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package wbrsolutions.mbcheckfx.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.math.BigDecimal;

/**
 *
 * @author Wellington Wagner (wellingtonwa@gmail.com)
 */
public class Compra {
    
    @JsonIgnore
    private BigDecimal preco;
    @JsonIgnore
    private BigDecimal volume;

    public Compra(final BigDecimal preco, final BigDecimal volume) {
        this.preco = preco;
        this.volume = volume;
    }

    public Compra() {
    }

    public BigDecimal getPreco() {
        return preco;
    }

    public void setPreco(final BigDecimal preco) {
        this.preco = preco;
    }

    public BigDecimal getVolume() {
        return volume;
    }

    public void setVolume(final BigDecimal volume) {
        this.volume = volume;
    }
    
    
}
