/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbrsolutions.mbcheckfx.model.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;

/**
 *
 * @author Wellington Wagner (wellingtonwa@gmail.com)
 */
public class HtmlUtil {

    private HtmlUtil() {
    }

    public static String getHtml(final String url) throws IOException{
        final URL oracle = new URL(url);
        final BufferedReader in = new BufferedReader(
                new InputStreamReader(oracle.openStream(), StandardCharsets.UTF_8));

        String inputLine;
        final StringBuilder json = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            json.append(inputLine);
        }
        in.close();
        return json.toString();
    }

}
