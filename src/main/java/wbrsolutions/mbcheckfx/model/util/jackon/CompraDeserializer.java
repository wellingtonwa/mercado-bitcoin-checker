/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package wbrsolutions.mbcheckfx.model.util.jackon;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import wbrsolutions.mbcheckfx.model.Compra;

/**
 *
 * @author Wellington Wagner (wellingtonwa@gmail.com)
 */
public class CompraDeserializer extends JsonDeserializer<List<Compra>>{

    @Override
    public List<Compra> deserialize(final JsonParser jp, final DeserializationContext dc) throws IOException {
        final JsonNode node = jp.getCodec().readTree(jp);
        final List<Compra> result = new ArrayList<>();
        for(int i=0;i<node.size();i++){
            final JsonNode auxNode = node.get(i);
            final Compra compra = new Compra();
            compra.setPreco(BigDecimal.valueOf(auxNode.get(0).asDouble()));
            compra.setVolume(BigDecimal.valueOf(auxNode.get(1).asDouble()));
            if(compra.getPreco()!=null){
                compra.setPreco(compra.getPreco().setScale(5, RoundingMode.HALF_EVEN));
                compra.setVolume(compra.getVolume().setScale(5, RoundingMode.HALF_EVEN));
            }
            result.add(compra);
        }
        return result;
    }

}
