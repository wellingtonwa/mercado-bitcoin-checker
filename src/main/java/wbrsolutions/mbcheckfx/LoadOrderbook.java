/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbrsolutions.mbcheckfx;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimerTask;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import wbrsolutions.mbcheckfx.model.Compra;
import wbrsolutions.mbcheckfx.model.Orderbook;
import wbrsolutions.mbcheckfx.model.Venda;
import wbrsolutions.mbcheckfx.model.enumerated.CoinEnum;

/**
 *
 * @author Wellington Wagner (wellingtonwa@gmail.com)
 */
public class LoadOrderbook extends TimerTask {

    private final TableView tableAsks;
    private final TableView tableBids;
    private final Label label;
    private final CoinEnum coinEnum;

    public LoadOrderbook(final CoinEnum coinEnum, final TableView tableAsks, final TableView tableBids, final Label label) {
        this.tableAsks = tableAsks;
        this.tableBids = tableBids;
        this.label = label;
        this.coinEnum = coinEnum;
    }

    @Override
    public void run() {
        final Orderbook orderbook = APIMercadoBitcoin.getOrderbook(coinEnum);
        final List<Venda> vendas = orderbook.getAsks();
        final List<Compra> compras = orderbook.getBids();
        try {
            final ObservableList<Venda> dadosVenda = FXCollections.observableArrayList(vendas);
            final ObservableList<Compra> dadosCompra = FXCollections.observableArrayList(compras);
            Platform.runLater(() -> {
                label.setText(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));
                tableAsks.setItems(dadosVenda);
                tableBids.setItems(dadosCompra);
            });
        } catch (final NullPointerException e) {
            label.setText("Sem rede");
        }
    }

}
