package wbrsolutions.mbcheckfx.util;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;

public class TrayNotification {

    public static void displayTray(final String message) throws AWTException{
        //Obtain only one instance of the SystemTray object
        final SystemTray tray = SystemTray.getSystemTray();

        //If the icon is a file
        final Image image = Toolkit.getDefaultToolkit().createImage("icon.png");
        //Alternative (if the icon is on the classpath):
        //Image image = Toolkit.getDefaultToolkit().createImage(getClass().getResource("icon.png"));

        final TrayIcon trayIcon = new TrayIcon(image, "Tray Demo");
        //Let the system resize the image if needed
        trayIcon.setImageAutoSize(true);
        //Set tooltip text for the tray icon
        trayIcon.setToolTip("System tray icon demo");
        tray.add(trayIcon);

        trayIcon.displayMessage("Mercado Bitcoin Checker", message, TrayIcon.MessageType.INFO);
    }

}
