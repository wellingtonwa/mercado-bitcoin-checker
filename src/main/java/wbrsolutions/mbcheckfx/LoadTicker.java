/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbrsolutions.mbcheckfx;

import java.math.RoundingMode;
import java.util.TimerTask;

import javafx.application.Platform;
import javafx.scene.control.Label;
import wbrsolutions.mbcheckfx.model.Ticker;
import wbrsolutions.mbcheckfx.model.enumerated.CoinEnum;

/**
 *
 * @author Wellington Wagner (wellingtonwa@gmail.com)
 */
public class LoadTicker extends TimerTask {

    private final Label lastPrice;
    private final Label highPrice;
    private final Label lowPrice;
    private final Label vol;
    private final CoinEnum coinEnum;

    public LoadTicker(final CoinEnum coinEnum, final Label lastPrice, final Label highPrice, final Label lowPrice, final Label vol) {
        this.lastPrice = lastPrice;
        this.highPrice = highPrice;
        this.lowPrice = lowPrice;
        this.vol = vol;
        this.coinEnum = coinEnum;
    }

    @Override
    public void run() {
        Platform.runLater(() -> {
            lastPrice.setText("-");
            highPrice.setText("-");
            lowPrice.setText("-");
            vol.setText("-");
        });
        final Ticker ticker = APIMercadoBitcoin.getTicker(coinEnum);

        if (ticker != null && ticker.getDate() != null) {
            final String lp = ticker.getLast().setScale(2, RoundingMode.HALF_EVEN).toPlainString();
            final String hp = ticker.getHigh().setScale(2, RoundingMode.HALF_EVEN).toPlainString();
            final String lop = ticker.getLow().setScale(2, RoundingMode.HALF_EVEN).toPlainString();
            final String v = ticker.getVol().setScale(3, RoundingMode.HALF_EVEN).toPlainString();
            Platform.runLater(() -> {
                lastPrice.setText(lp);
                highPrice.setText(hp);
                lowPrice.setText(lop);
                vol.setText(v);
            });
        }
    }

}
