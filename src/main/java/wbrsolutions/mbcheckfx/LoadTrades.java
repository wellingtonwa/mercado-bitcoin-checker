/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbrsolutions.mbcheckfx;

import java.util.List;
import java.util.TimerTask;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableView;
import wbrsolutions.mbcheckfx.model.Trade;
import wbrsolutions.mbcheckfx.model.enumerated.CoinEnum;

/**
 *
 * @author Wellington Wagner (wellingtonwa@gmail.com)
 */
public class LoadTrades extends TimerTask {

    private final TableView tableView;
    private final CoinEnum coinEnum;

    public LoadTrades(final CoinEnum coinEnum, final TableView<?> tableView) {
        this.tableView = tableView;
        this.coinEnum = coinEnum;
    }

    @Override
    public void run() {
        final List<Trade> trades = APIMercadoBitcoin.getTrades(coinEnum);

        if (trades!=null && !trades.isEmpty()) {
            final ObservableList<Trade> dados = FXCollections.observableArrayList(trades);
            Platform.runLater(() -> tableView.setItems(dados));
        }
    }

}
