package wbrsolutions.mbcheckfx;

import java.math.BigDecimal;
import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.Timer;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import wbrsolutions.mbcheckfx.model.Compra;
import wbrsolutions.mbcheckfx.model.Trade;
import wbrsolutions.mbcheckfx.model.Venda;
import wbrsolutions.mbcheckfx.model.enumerated.CoinEnum;

public class FXMLController implements Initializable {

    public static final int PERIOD = 1000 * 10;
    public static final int DELAY = 0;
    public static final String VOLUME = "volume";
    public static final String PRECO = "preco";
    public static final String DATA = "data";
    public static final String TIPO = "tipo";
    public static final String AMOUNT = "amount";
    public static final String PRICE = "price";
    @FXML
    private Label lblHoraAtualizacao;
    @FXML
    private TableView<?> tbvOrdemCompraLitecoin;
    @FXML
    private TableView<?> tbvOrdemVendaLitecoin;
    @FXML
    private TableView<?> tbvTradesLitecoin;
    @FXML
    private TableView<?> tbvOrdemCompraBitcoin;
    @FXML
    private TableView<?> tbvOrdemVendaBitcoin;
    @FXML
    private TableView<?> tbvTradesBitcoin;
    @FXML
    private TableView<?> tbvOrdemCompraRipple;
    @FXML
    private TableView<?> tbvOrdemVendaRipple;
    @FXML
    private TableView<?> tbvTradesRipple;
    @FXML
    private TableColumn<Compra, BigDecimal> compraVolumeLitecoin;
    @FXML
    private TableColumn<Compra, BigDecimal> compraValorLitecoin;
    @FXML
    private TableColumn<Venda, BigDecimal> vendaVolumeLitecoin;
    @FXML
    private TableColumn<Venda, BigDecimal> vendaValorLitecoin;
    // Colunas de Compra/Venda Bitcoin
    @FXML
    private TableColumn<Compra, BigDecimal> compraVolumeBitcoin;
    @FXML
    private TableColumn<Compra, BigDecimal> compraValorBitcoin;
    @FXML
    private TableColumn<Venda, BigDecimal> vendaVolumeBitcoin;
    @FXML
    private TableColumn<Venda, BigDecimal> vendaValorBitcoin;
    // Colunas de Compra/Venda Ripple
    @FXML
    private TableColumn<Compra, BigDecimal> compraVolumeRipple;
    @FXML
    private TableColumn<Compra, BigDecimal> compraValorRipple;
    @FXML
    private TableColumn<Venda, BigDecimal> vendaVolumeRipple;
    @FXML
    private TableColumn<Venda, BigDecimal> vendaValorRipple;
    // Columns of trade bitcoin tableview
    @FXML
    private TableColumn<Trade, Date> dateTradeBitcoin;
    @FXML
    private TableColumn<Trade, String> typeTradeBitcoin;
    @FXML
    private TableColumn<Trade, BigDecimal> amountTradeBitcoin;
    @FXML
    private TableColumn<Trade, BigDecimal> priceTradeBitcoin;
    // Columns of trade litecoin tableview
    @FXML
    private TableColumn<Trade, Date> dateTradeLitecoin;
    @FXML
    private TableColumn<Trade, String> typeTradeLitecoin;
    @FXML
    private TableColumn<Trade, BigDecimal> amountTradeLitecoin;
    @FXML
    private TableColumn<Trade, BigDecimal> priceTradeLitecoin;
    // Columns of trade Ripple tableview
    @FXML
    private TableColumn<Trade, Date> dateTradeRipple;
    @FXML
    private TableColumn<Trade, String> typeTradeRipple;
    @FXML
    private TableColumn<Trade, BigDecimal> amountTradeRipple;
    @FXML
    private TableColumn<Trade, BigDecimal> priceTradeRipple;

    @FXML
    private Label highPriceLitecoin;
    @FXML
    private Label lowPriceLitecoin;
    @FXML
    private Label lastPriceLitecoin;
    @FXML
    private Label volLitecoin;
    @FXML
    private Label highPriceBitcoin;
    @FXML
    private Label lowPriceBitcoin;
    @FXML
    private Label lastPriceBitcoin;
    @FXML
    private Label volBitcoin;
    @FXML
    private Label highPriceRipple;
    @FXML
    private Label lowPriceRipple;
    @FXML
    private Label lastPriceRipple;
    @FXML
    private Label volRipple;

    private final Timer timer = new Timer(true);

    @Override
    public void initialize(final URL url, final ResourceBundle rb) {
        compraValorLitecoin.setCellValueFactory(new PropertyValueFactory<>(PRECO));
        compraVolumeLitecoin.setCellValueFactory(new PropertyValueFactory<>(VOLUME));
        vendaValorLitecoin.setCellValueFactory(new PropertyValueFactory<>(PRECO));
        vendaVolumeLitecoin.setCellValueFactory(new PropertyValueFactory<>(VOLUME));
        compraValorBitcoin.setCellValueFactory(new PropertyValueFactory<>(PRECO));
        compraVolumeBitcoin.setCellValueFactory(new PropertyValueFactory<>(VOLUME));
        vendaValorBitcoin.setCellValueFactory(new PropertyValueFactory<>(PRECO));
        vendaVolumeBitcoin.setCellValueFactory(new PropertyValueFactory<>(VOLUME));
        // Ripple
        compraValorRipple.setCellValueFactory(new PropertyValueFactory<>(PRECO));
        compraVolumeRipple.setCellValueFactory(new PropertyValueFactory<>(VOLUME));
        vendaValorRipple.setCellValueFactory(new PropertyValueFactory<>(PRECO));
        vendaVolumeRipple.setCellValueFactory(new PropertyValueFactory<>(VOLUME));
        //Bitcoin trades
        dateTradeBitcoin.setCellValueFactory(new PropertyValueFactory<>(DATA));
        typeTradeBitcoin.setCellValueFactory(new PropertyValueFactory<>(TIPO));
        amountTradeBitcoin.setCellValueFactory(new PropertyValueFactory<>(AMOUNT));
        priceTradeBitcoin.setCellValueFactory(new PropertyValueFactory<>(PRICE));
        //Litecoin trades
        dateTradeLitecoin.setCellValueFactory(new PropertyValueFactory<>(DATA));
        typeTradeLitecoin.setCellValueFactory(new PropertyValueFactory<>(TIPO));
        amountTradeLitecoin.setCellValueFactory(new PropertyValueFactory<>(AMOUNT));
        priceTradeLitecoin.setCellValueFactory(new PropertyValueFactory<>(PRICE));
        //Ripple trades
        dateTradeRipple.setCellValueFactory(new PropertyValueFactory<>(DATA));
        typeTradeRipple.setCellValueFactory(new PropertyValueFactory<>(TIPO));
        amountTradeRipple.setCellValueFactory(new PropertyValueFactory<>(AMOUNT));
        priceTradeRipple.setCellValueFactory(new PropertyValueFactory<>(PRICE));

        preencherTabelasNegociacoes();
        preencherTicker();
        preencherTrades();
    }

    public void preencherTabelasNegociacoes() {
        timer.scheduleAtFixedRate(
                new LoadOrderbook(CoinEnum.LITECOIN, tbvOrdemVendaLitecoin, tbvOrdemCompraLitecoin, lblHoraAtualizacao), DELAY, PERIOD);
        timer.scheduleAtFixedRate(new LoadOrderbook(CoinEnum.BITCOIN, tbvOrdemVendaBitcoin, tbvOrdemCompraBitcoin, lblHoraAtualizacao),
                                  DELAY, PERIOD);
        timer.scheduleAtFixedRate(new LoadOrderbook(CoinEnum.RIPPLE, tbvOrdemVendaRipple, tbvOrdemCompraRipple, lblHoraAtualizacao),
                                  DELAY, PERIOD);
    }

    public void preencherTicker() {
        timer.scheduleAtFixedRate(
                new LoadTicker(CoinEnum.LITECOIN, lastPriceLitecoin, highPriceLitecoin, lowPriceLitecoin, volLitecoin), DELAY, PERIOD);
        timer.scheduleAtFixedRate(new LoadTicker(CoinEnum.BITCOIN, lastPriceBitcoin, highPriceBitcoin, lowPriceBitcoin, volBitcoin),
                                  DELAY, PERIOD);
        timer.scheduleAtFixedRate(new LoadTicker(CoinEnum.RIPPLE, lastPriceRipple, highPriceRipple, lowPriceRipple, volRipple), DELAY,
                                  PERIOD);
    }

    public void preencherTrades() {
        timer.scheduleAtFixedRate(new LoadTrades(CoinEnum.BITCOIN, tbvTradesBitcoin), DELAY, PERIOD);
        timer.scheduleAtFixedRate(new LoadTrades(CoinEnum.LITECOIN, tbvTradesLitecoin), DELAY, PERIOD);
        timer.scheduleAtFixedRate(new LoadTrades(CoinEnum.RIPPLE, tbvTradesRipple), DELAY, PERIOD);
    }


}
